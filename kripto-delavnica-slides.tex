\documentclass[a4paper, 12pt]{beamer}

\usepackage[slovene]{babel}

\usetheme{CambridgeUS}
\usecolortheme{beaver}
\usefonttheme{structuresmallcapsserif}

\definecolor{bostonuniversityred}{rgb}{0.8, 0.0, 0.0}

\newenvironment{matematika}[1]{
\textcolor{bostonuniversityred}{\underline{\textsc{#1:}}}
}{
}

\input{basic}
\input{math}

\title{Uvod v (post-kvantno) kriptografijo}
\author{Benjamin Benčina}
\institute[UL FMF]{Univerza v Ljubljani, Fakulteta za matematiko in fiziko}
\date{2022-09-09}

\begin{document}

\titlepage

\begin{frame}
    \frametitle{Vsebina}
    \begin{itemize}[label=\ding{227}]
        \item Zgodovinski uvod.
            \vfill
        \item Kriptosistem RSA:
            \begin{itemize}
                \item konstrukcija sistema,
                \item napadi nanj,
                \item kvantna nevarnost.
            \end{itemize}
            \vfill
        \item Kriptosistem AGCD:
            \begin{itemize}
                \item učenje z napako,
                \item konstrukcija sistema,
                \item napad nanj,
                \item računanje na šifrah.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Zgodovina kriptografije}
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.4]{pics/scytale.png}
        \caption{Skitala -- ``military-grade'' šifriranje \textasciitilde2700 let nazaj.}
        \label{fig:scytale}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Zgodovina kriptografije}
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.3]{pics/caesar.png}
        \caption{Cezarjanka -- ``military-grade'' šifriranje \textasciitilde2100 let nazaj.}
        \label{fig:caesar}
    \end{figure}
    % cezarjanka (ROT13) + slikca kroga
    % VAJA - kako razbiti cezarjanko? + DEMO
\end{frame}

\begin{frame}
    \frametitle{Zgodovina kriptografije}
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.4]{pics/frequency.png}
        \caption{Frekvence angleških črk 9-te izdaje \emph{Concise Oxford Dictionary}. \cite{bib:freq}}
        \label{fig:frequency}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Zgodovina kriptografije}
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.3]{pics/vigenere.png}
        \caption{Vigen\`erova šifra (1553) -- prvič razbita 1854. \cite{bib:vigenere}}
        \label{fig:vigenere}
    \end{figure}
    % vigenere DN - kako bi razbili vigenerovo šifro (namig: najprej dolžina ključa) + DEMO
\end{frame}

\begin{frame}
    \frametitle{Kriptografija z javnim ključem}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Simetrična kriptografija -- ključ za šifriranje je enak
            ključu za dešifriranje.
            \vfill
        \item<2-> Asimetrična kriptografija -- ključ za šifriranje in
            dešifriranje sta povezana s skrivno informacijo.
            \vfill
        \item<3-> Ključ za šifriranje objavimo, ključ za dešifriranje pa
            zadržimo zase. Stavimo na to, da je težko izračunati skrivni ključ
            iz javnega brez skrivne informacije (ki jo vemo le mi).
            \vfill
        \item<4-> Zanimiv postane študij \emph{računsko težkih problemov}, ki
            jih lahko uporabimo za asimetrično kriptografijo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Kriptografija z javnim ključem}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Težki problemi: faktorizacija celih števil (RSA), diskretni
            logaritem (DH), diskretni logaritem na eliptični krivulji (ECDH).
            \vfill
        \item<2-> Nobeden od zgornjih problemov ni varen proti napadu s kvatnim
            računalnikom.
            \vfill
        \item<3-> Peter Shor leta 1994 objavi \emph{kvantni algoritem} za
            faktorizacijo celih števil.
            \vfill
        \item<4-> Težava: kvantni računalniki lahko za nazaj dešifrirajo vso
            internetno komunikacijo -- prehod na kvantno varne sisteme.
    \end{itemize}
    % kvantni računalniki bad, napovemo shorov algoritem
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item Modularna ali urna aritmetika po modulu $n$ -- ko gremo preko
            $n$, le zakrožimo nazaj do $0$.
            \vfill
        \item Množica $\Z_n = \left\{ 0, 1,\dots,n-1 \right\}$.
            \vfill
        \item Prilagodimo seštevanje in množenje.
    \end{itemize}
    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.1]{pics/clock-arithmetic.png}
        \caption{Kosilo bo ob \emph{enih}. \cite{bib:clock}}
        \label{fig:clock-arithmetic}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1-> {
        \begin{matematika}{Definicija}
            Eulerjeva funkcija $\varphi(n)$ vrne število vseh naravnih števil, ki
            so manjša od $n$ in številu $n$ tuja. Torej:
            \[
                \varphi(n) = \#\left\{ x \in \Z_n ; \; \text{GCD}(x, n) = 1 \right\}.
            \]
        \end{matematika}
    }
    \visible<2-> {
        Ta števila so točno tisti $x \in \Z_n$, da obstaja $y \in \Z_n$, da $xy =
        1\ (\text{mod } n)$.
    }
    \vfill
    \visible<3-> {
        \begin{matematika}{Bezoutova identiteta}
            Za vsaki števili $a, b \in \Z$ obstajata tuji števili $x, y \in \Z$, da
            velja
            \[
                a \cdot x + b \cdot y = GCD(a, b).
            \]
        \end{matematika}
    }
    \vfill
    \visible<4-> {
        \begin{matematika}{Eulerjev izrek}
            Za tuji števili $x$ in $n$ velja $x^{\varphi(n)} = 1\ (\text{mod } n)$.
        \end{matematika}
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1->{
        \begin{matematika}{Problem}
            Naj bo $n \in \N$ dano število. Poišči njegove prafaktorje, tj.
            praštevila $p_1,\dots,p_m$, da je $n = p_1\cdots p_m$.
        \end{matematika}
    }
    \vfill
    \visible<2->{
        \begin{matematika}{Problem}
            Naj bo $n \in \N$ dano število, ki je oblike $n = p \cdot q$, kjer sta
            $p$ in $q$ praštevili. Določi $p$ (in $q$).
        \end{matematika}
    }
    \vfill
    \visible<3->{
        \begin{matematika}{Problem}
            Naj bo $n \in \N$ dano število. Ali je $n$ praštevilo?
        \end{matematika}
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{matematika}{Algoritem (Rivest, Shamir, Adleman -- 1977)}
        \begin{itemize}[label=\ding{227}]
            \item<1-> Izberemo dve različni praštevili $p$ in $q$.
                \vfill
            \item<2-> Izračunamo $n = p\cdot q$ in $\varphi(n) = (p-1)\cdot(q-1)$.
                \vfill
            \item<3-> Izberemo število $e$, ki je tuje številu $\varphi(n)$.
                \vfill
            \item<4-> \textbf{Javni ključ} tvori par $(n, e)$.
                \vfill
            \item<5-> Z Evklidovim algoritmom izračunamo tako število $d$, da je $e \cdot d = 1\ (\text{mod } \varphi(n))$.
                \vfill
            \item<6-> \textbf{Zasebni ključ} tvori par $(n, d)$.
                \vfill
            \item<7-> \textbf{Šifriranje:} $c = m^e \text{ mod } n$.
                \vfill
            \item<8-> \textbf{Dešifriranje:} $m = c^d \text{ mod } n$.
        \end{itemize}
    \end{matematika}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{matematika}{Izrek (RSA)}
        Naj bodo $n = p \cdot q$, $e$ in $d$ kot zgoraj in naj bo $m < n$
        skrito sporočilo. Tedaj velja
        \[
            m^{e\cdot d} = m\ (\text{mod } n),
        \]
        tj. algoritem RSA je pravilen.
    \end{matematika}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Dokaz na tablo.
            \vfill
        \item<2-> Demo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Primer ključev.
            \vfill
        \item<2->  Stran o RSA certifikatih \cite{bib:rsacert} ima sama RSA certifikat.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Pozor!
            \vfill
        \item<2-> Čeprav je matematično z algoritmom vse v redu, lahko
            malomarnost in slaba implementacija povzročata težave.
            \vfill
        \item<3-> Oglejmo si nekaj primerov.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1->{
        \underline{Recimo, da poznamo $\varphi(n)$. Kako bi faktorizirali $n$?}
    }
    \vfill
    \visible<2->{
        \[
            n = p \cdot q
        \]
        \[
            \varphi(n) = (p-1) \cdot (q-1) = pq - (p+q) + 1
        \]
        Poznamo torej $p\cdot q = n$ in
        \[
            p + q = n + 1 - \varphi(n).
        \]
        Vietove formule: $p$ in $q$ sta rešitvi kvadratne enačbe
        \[
            x^2 - (p+q)x + p \cdot q = 0.
        \]
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1->{
        \uline{Recimo, da vemo, da sta si $p$ in $q$ blizu skupaj. Kako bi hitreje faktorizirali $n$?}
    }
    \vfill
    \visible<2->{
        BŠZS: $p > q$. Poskusimo s \emph{Fermatovo faktorizacijsko metodo}.
        \[
            n = p \cdot q = \left( \frac{p + q}{2}  \right)^2 - \left( \frac{p - q}{2} \right)^2.
        \]
        Ker sta si $p$ in $q$ blizu, je $s = \frac{p-q}{2}$ majhno število, $t
        = \frac{p+q}{2}$ pa je le malo večje število od $\sqrt{n}$.
        V enačbo
        \[
            t^2 - n = s^2
        \]
        vstavljamo $t = \lceil \sqrt{n} \rceil + k$ za $k = 0, 1, 2,\dots$,
        dokler ne dobimo na desni popolnega kvadrata.
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1->{
        \uline{Recimo, da vemo, da sta dva javna ključa $n_1$, in $n_2$ osnovana na istem praštevilu. Lahko faktoriziramo $n_1$ in $n_2$?}
    }
    \vfill
    \visible<2->{
        \begin{align*}
            p &= GCD(n_1, n_2) \\
            q_1 &= \frac{n_1}{p} \\
            q_2 &= \frac{n_2}{p}
        \end{align*}
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \visible<1->{
        \underline{Recimo, da sta $m$ in $e$ tako majhna, da velja $m^e < n$. Kako dobiti $m$?}
    }
    \vfill
    \visible<2->{
        Ker $c = m^e < n$, potem modulo z $n$ ne naredi ničesar, zato lahko
        preprosto korenimo:
        \[
            m = c^{\frac{1}{e}}
        \]
    }
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{matematika}{Kitajski izrek o ostankih}
        Naj bodo $n_1,\dots,n_k \in \N$ paroma tuja števila in $a_i \in
        \Z_{n_i}$ za vsak $i=1,\dots,k$. Označimo z $N = n_1\cdots n_k$.
        Tedaj obstaja enolično določena rešitev sistema
        \begin{align*}
            x &= a_1\ (\text{mod } n_1) \\
            &\vdots \\
            x &= a_k\ (\text{mod } n_k)
        \end{align*}
        do $(\text{mod } N)$ natančno.
    \end{matematika}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Skica dokaza na tablo.
            \vfill
        \item<2-> Demo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{matematika}{Algoritem (Shor -- 1994)}
        \begin{itemize}[label=\ding{227}]
            \item Izberemo poljuben $a \in \Z_n\setminus\{1, 2\}$.  BŠZS: $a$
                in $n$ tuji števili.
                \vfill
            \item Izračunamo periodo $r$ števila $a$, tj. najmanjše število
                $r$, da velja $a^r = 1\ (\text{mod } n)$. Ponavljamo koraka
                dokler število $r$ ni sodo.
                \vfill
            \item Velja
                \[
                    a^r - 1 = \left( a^{\frac{r}{2}} - 1 \right)\cdot\left( a^{\frac{r}{2}} + 1 \right) = 0\ (\text{mod } n)
                \]
                \vfill
            \item Po predpostavki o minimalnosti periode $t = a^{\frac{r}{2}} -
                1$ ni večkratnik števila $n$. Če je $s = a^{\frac{r}{2}} + 1$
                večkratnik $n$, se vrnemo na prvi korak.
                \vfill
            \item Sicer mora $p$ deliti $t$ in $q$ deliti $s$, torej ju izračunamo
                z $GCD(n, t)$ in $GCD(n, s)$.
        \end{itemize}
    \end{matematika}
\end{frame}

\begin{frame}
    \frametitle{RSA}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Faktorizacija naravnega števila $n$ je lahka, če znamo hitro
            poiskati \emph{periodo poljubnega števila}, tj. za vsak $a \in
            \Z_n$ iščemo najmanjši tak $r \in \N$, da je $a^r = 1\ (\text{mod }
            n)$. \cite{bib:shoralgo}
            \vfill
        \item<2-> Bistvo Shorovega algoritma je algoritem za iskanje periode s
            \emph{kvantno Fourierjevo transformacijo}.
    \end{itemize}
\end{frame}

% \begin{frame}
%     \frametitle{AGCD}
%     \begin{figure}[h]
%         \centering
%         \includegraphics[scale=0.25]{pics/rsa-mouth-shut.jpg}
%         \label{fig:rsameme}
%     \end{figure}
% \end{frame}

\begin{frame}
    \frametitle{AGCD}
    \visible<1->{
        \begin{matematika}{Definicija}
            Naj bo $f$ neznana funkcija in $V = \left\{
                (x_1,y_1),\dots,(x_k,y_k) \right\}$ množica parov, za katere
            velja $y_i = f(x_i)$ za $i = 1,\dots,k$. Algoritem \emph{se je
            naučil} funkcijo $f$, če je iz poljubne dovolj velike množice $V$
            zmožen za vsak $x$ vrniti $y = f(x)$.
        \end{matematika}
    }
    \vfill
    \visible<2->{
        \begin{matematika}{Definicija}
            Naj bo $f$ neznana funkcija in $V = \left\{
                (x_1,y_1),\dots,(x_k,y_k) \right\}$ množica parov, za katere
            velja $y_i = f(x_i) + e_i$ za majhne napake $e_i$ in $i =
            1,\dots,k$. Algoritem \emph{se je naučil} funkcijo $f$ \emph{z
            napako}, če je iz poljubne dovolj velike množice $V$ zmožen za vsak
            $x$ vrniti $y = f(x)$.
        \end{matematika}
    }
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    Vrnimo se nazaj na primer sistema RSA, kjer si dva javna ključa delita
    praštevilo, torej
    \begin{align*}
        n_1 &= p \cdot q_1 \\
        n_2 &= p \cdot q_2 \\
        &\ \vdots
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    \visible<1->{
        \begin{matematika}{Definicija}
            Naj bo $p$ skrito praštevilo in $q_1,\dots,q_k,r_1,\dots,r_k$
            naključno izbrana (neznana) naravna števila, kjer so
            $r_1,\dots,r_k$ majhni v primerjavi s $p$ in $q_1,\dots,q_k$.
            \emph{Problem približnega največjega skupnega delitelja} je ob
            danih
            \begin{align*}
                c_1 &= q_1 \cdot p + r_1 \\
                &\ \vdots \\
                c_k &= q_k \cdot p + r_k
            \end{align*}
            najti praštevilo $p$.
        \end{matematika}
    }
    \vfill
    \visible<2->{
        \begin{center}
            ``Množenje z napako.''
        \end{center}
    }
    \vfill
    \visible<3->{
        Problem AGCD je težek tudi na kvantnem računalniku.
    }
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    \begin{matematika}{Algoritem (AGCD)}
        \begin{itemize}[label=\ding{227}]
            \item<1-> Izberemo dovolj veliko praštevilo $p$, ki predstavlja naš
                skrivni ključ za dešifriranje.
                \vfill
            \item<1-> S praštevilom $p$ generiramo veliko število vzorcev AGCD
                $C = \left\{ c_1,\dots,c_k \right\}$, kjer so vse napake
                $r_1,\dots,r_k$ soda števila.
                \vfill
            \item<2-> Da šifriramo skrivno sporočilo $m \in \left\{ 0, 1
                \right\}$ (en bit), generiramo naključnih $k$ bitov
                $b_1,\dots,b_k$ in izračunamo
                \[
                    e = m + \sum_{i = 1}^{k} b_i\cdot c_i = m + b_1\cdot c_1 + \cdots + b_k \cdot c_k.
                \]
                \vfill
            \item<3-> Za dešifriranje izračunamo
                \[
                    d = (e \text{ mod } p) \text{ mod } 2.
                \]
        \end{itemize}
    \end{matematika}
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Zgornji kriptosistem se zanaša na to, da naš generator
            naključnih bitov ni pristranski!
            \vfill
        \item<2-> Če je PRNG nepristranski, mora napadalec preveriti vseh $2^k$
            kombinacij vzorcev.
            \vfill
        \item<3-> Če pa PRNG ob vsakem šifriranju spremeni le kakšen bit, lahko
            zaporedoma preverjamo najprej $\binom{k}{1}$ kombinacij, nato
            $\binom{k}{2}$ itd., odvisno koliko bitov se razlikuje.
    \end{itemize}
    % VAJA - poor randomness attack na AGCD
    % DEMO - poor randomness attack na AGCD
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    Po binomskem izreku velja
    \[
        2^k = \sum_{i = 0}^{k} \binom{k}{i}
    \]
    Če se ob vsakem šifriranju spremeni le kakšen bit in moramo preveriti
    kombinacije javnih parametrov do $m << k$, potem
    \[
        2^k = \underbrace{\binom{k}{0} + \cdots + \binom{k}{m}}_\text{preverimo} + \underbrace{\binom{k}{m+1} + \cdots + \binom{k}{k}}_\text{prihranimo}
    \]
\end{frame}

\begin{frame}
    \frametitle{AGCD}
    \begin{itemize}[label=\ding{227}]
        \item<1-> Homomorfno šifriranje -- lahko računamo na šifriranih
            podatkih, ne da bi jih dešifrirali.
            \vfill
        \item<2-> Zgornji AGCD sistem je \emph{povsem homomorfen}, tj.
            izračunamo lahko vsako logično in s tem aritmetično vrednost.
    \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
    \frametitle{Viri}
    
    \begin{thebibliography}{99}
        \bibitem{bib:codebook}
            Singh S.
            \textit{The code book},
            Four Estate Ltd, 2003
        \bibitem{bib:fhe}
            Cheon, J.H., Stehl\'e, D.
            \textit{Fully Homomophic Encryption over the Integers Revisited}.
            In: Oswald, E., Fischlin, M. (eds) Advances in Cryptology -- EUROCRYPT 2015. EUROCRYPT 2015.
            Lecture Notes in Computer Science, Vol 9056.
            Springer, Berlin, Heidelberg.
            \url{https://doi.org/10.1007/978-3-662-46800-5_20}
        \bibitem{bib:malb}
            \url{https://martinralbrecht.wordpress.com/2020/03/21/the-approximate-gcd-problem/}
        \bibitem{bib:tkk}
            \url{https://gitlab.com/bbencina/TKK-algoritmi}
        \bibitem{bib:shoralgo}
            \url{https://quantum-computing.ibm.com/composer/docs/iqx/guide/shors-algorithm}
        \bibitem{bib:freq}
            \url{https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html}
        \bibitem{bib:vigenere}
            \url{https://www.kaspersky.com/blog/vigenere-cipher-history/9802/}
        \bibitem{bib:clock}
            \url{https://en.wikipedia.org/wiki/Modular_arithmetic}
        \bibitem{bib:rsacert}
            \url{https://sectigostore.com/page/what-is-an-rsa-certificate/}
    \end{thebibliography}
\end{frame}

\end{document}
