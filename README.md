# Uvod v (post-kvantno) kriptografijo

## Napovednik

Uvodna predavanja in knjige o kriptografiji z javnim ključem se ponavadi
začnejo s kriptosistemom RSA, saj ga je v njegovi osnovni obliki tako lahko
razložiti. Čeprav je širom uporabljen na internetu, ima kriptosistem RSA mnogo
težav že v klasičnem smislu, povsem pa podleže tudi napadu s kvantnim
računalnikom. V prvem delu delavnice si bomo podrobno ogledali kriptosistem RSA
in nekaj njegovih zabavnejših šibkosti. V drugem delu bomo eno od šibkosti le
malo spremenili in dobili kvantno težek računski problem, t.i. problem
približnega največjega skupnega delitelja, ter na njegovi osnovi sestavili
model post-kvantnega kriptosistema.
