import time

ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z',]

L = len(ALPHABET)

# frequency of letters in the English language (in %)
# source: http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
FREQUENCY = {'A': 8.12, 'B': 1.49, 'C': 2.71, 'D': 4.32, 'E': 12.02, 'F': 2.3,
             'G': 2.03, 'H': 5.92, 'I': 7.31, 'J': 0.1, 'K': 0.69, 'L': 3.98,
             'M': 2.61, 'N': 6.95, 'O': 7.68, 'P': 1.82, 'Q': 0.11, 'R': 6.02,
             'S': 6.28, 'T': 9.10, 'U': 2.88, 'V': 1.11, 'W': 2.09, 'X': 0.17,
             'Y': 2.11, 'Z': 0.07}

def getNum(letter):
        '''Returns index of letter in the alphabet, starting with 0.'''
        return ord(letter) - ord('A')

def CaesarEncrypt(plaintext, offset):
    ciphertext = ''
    for char in plaintext:
        if char not in ALPHABET:
            ciphertext += char
        else:
            index_of_char = getNum(char)
            new_char = ALPHABET[(index_of_char + offset) % L]
            ciphertext += new_char
    return ciphertext

def CaesarDecrypt(ciphertext, offset):
    plaintext = ''
    for char in ciphertext:
        if char not in ALPHABET:
            plaintext += char
        else:
            index_of_char = getNum(char)
            new_char = ALPHABET[(L + index_of_char - offset) % L]
            plaintext += new_char
    return plaintext

def mostCommonLetter(text):
    counts = {}
    for char in text:
        if char not in ALPHABET:
            continue
        if char not in counts:
            counts[char] = 1
        else:
            counts[char] += 1
    maxcount = ''
    maxnum = 0
    for char in counts:
        if counts[char] >= maxnum:
            maxcount = char
            maxnum = counts[char]
    return maxcount

PLAINTEXT = """THE FREQUENCY OF LETTERS IN TEXT HAS BEEN STUDIED
FOR USE IN CRYPTANALYSIS, AND FREQUENCY ANALYSIS IN PARTICULAR,
DATING BACK TO THE ARAB MATHEMATICIAN AL-KINDI (C. 801–873 AD),
WHO FORMALLY DEVELOPED THE METHOD (THE CIPHERS BREAKABLE BY THIS
TECHNIQUE GO BACK AT LEAST TO THE CAESAR CIPHER INVENTED BY JULIUS
CAESAR, SO THIS METHOD COULD HAVE BEEN EXPLORED IN CLASSICAL TIMES).
LETTER FREQUENCY ANALYSIS GAINED ADDITIONAL IMPORTANCE IN EUROPE
WITH THE DEVELOPMENT OF MOVABLE TYPE IN 1450 AD, WHERE ONE MUST
ESTIMATE THE AMOUNT OF TYPE REQUIRED FOR EACH LETTERFORM, AS
EVIDENCED BY THE VARIATIONS IN LETTER COMPARTMENT SIZE IN
TYPOGRAPHER'S TYPE CASES."""

def main():
    print("--- PLAINTEXT ---")
    print(PLAINTEXT)
    print("")
    time.sleep(5)
    print("--- Encrypting with offset: 13 ---")
    c = CaesarEncrypt(PLAINTEXT, 13)
    print(c)
    print("")
    time.sleep(5)
    print("--- Finding most common letter: ---")
    letter = mostCommonLetter(c)
    print(letter)
    print("")
    time.sleep(5)
    print("--- Finding key: ---")
    key = getNum(letter) - getNum(mostCommonLetter(PLAINTEXT))
    print(letter + " - E = " + str(key))
    print("")
    time.sleep(5)
    print("--- Decrypting: ---")
    print(CaesarDecrypt(c, key))

if __name__ == "__main__":
    main()
