# THIS IS ONLY FOR DEMO PURPOSES, USE PROPER PRNG FOR REAL USES
import random
import time

def AGCDGen(p):
    q = random.randrange(10000, 100000)
    r = random.randrange(10, 100, 2)
    return q * p + r

def AGCDEncrypt(m, C):
    bs = []
    for i in range(len(C)):
        b = random.randrange(0, 2)
        bs.append(b)
    s = m
    for i in range(len(C)):
        s += bs[i] * C[i]
    return s

def AGCDDecrypt(e, p):
    return (e % p) % 2

random.seed()

p = int(input("Input your favourite prime: "))
k = int(input("How many samples should be in the key: "))

print("Generating samples...")
time.sleep(2)
C = []
for i in range(k):
    C.append( AGCDGen(p))
print(C)

m = int(input("Which bit would you like to encrypt: "))
if m != 1 and m != 0:
    print("Bits can only have 0 1 values.")
    exit(1)

print("Encrypting...")
time.sleep(5)

e = AGCDEncrypt(m, C)

print("Encrypted message: ", e)

print("Decrypting...")
time.sleep(5)

d = AGCDDecrypt(e, p)

print("Decrypted bit: ", d)


