# THIS IS ONLY FOR DEMO PURPOSES, USE PROPER PRNG FOR REAL USES
import random
import time
import math
import sys

def AGCDGen(p, L):
    q = random.randrange(pow(2, int(L * math.log(L, 2))), pow(2, (int(L * math.log(L, 2) + 1))))
    r = random.randrange(pow(2, L), pow(2, L+1), 2)
    return q * p + r

def badBits(n, seed):
    if n > 256:
        print("Too many bits requested, 256 is more than enough")
        exit(1)
    bs = seed[:n]
    i = random.randrange(0,n)
    print("Seed index: ", i)
    if bs[i] == 0:
        bs[i] = 1
    elif bs[i] == 1:
        bs[i] = 0
    else:
        print("Not a bit")
        exit(1)
    return bs

def AGCDEncrypt(m, C):
    #  bs = []
    #  for i in range(len(C)):
        #  b = random.randrange(0, 2)
        #  bs.append(b)
    bs = badBits(len(C), bseed)
    s = m
    for i in range(len(C)):
        s += bs[i] * C[i]
    return s

def AGCDDecrypt(e, p):
    return pow(pow(e, 1, p), 1, 2)

random.seed()

#  L = int(input("Input the security parameter: "))
#  k = int(input("How many samples should be in the key: "))
L = 160
k = 160

print("L = ", L)
print("k = ", k)

time.sleep(5)

print("Generating prime with L+log(L) bits (actually hardcoded, oops)...")
#  p = largePrime(int(L + math.log(L, 2)), mrt=10000)
p = 359334085968622831041960188598043661065388726959079837
print("Prime: ", p)

time.sleep(5)

print("Generating samples...")
#  time.sleep(5)
C = []
for i in range(k):
    C.append(AGCDGen(p, L))

time.sleep(5)
#  print(C)

#  one = 0
#  zero = 0
#  for i in range(100000):
    #  sys.stdout.write('\rTest: ' + str(i))
    #  sys.stdout.flush()
    #  m = random.randint(0, 1)
    #  if m == 0:
        #  zero += 1
    #  if m == 1:
        #  one += 1
    #  e = AGCDEncrypt(m, C)
    #  d = AGCDDecrypt(e, p)
    #  if m != d:
        #  print("\nError!")
        #  exit(1)
#  print()
#  print("One: ", one)
#  print("Zero: ", zero)
#  print("Encrypting...")
#  time.sleep(5)

bseed = [1 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 1 , 1 , 1 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 0 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 1 , 1 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 1 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 1 , 1 , 0 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 1 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 1 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 0 , 0 , 0 , 1 , 1 , 0 , 0 , 1 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 0 , 1 , 0 , 1 , 0 , 1 , 1 , 1 , 0 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 1 , 0 , 1 , 1 , 0 , 0 , 0 , 0 , 1 , 0 , 1 , 0 , 0 , 1 , 0 , 0 , 0 , 0 , 1 , 1 , 0 , 0 , 1 , 0 , 1 , 1 , 1 , 1 , 0 , 1 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 0 , 1 , 1 , 0 , 0 , 0 , 1 , 0 , 0 , 1 , 1 , 1 , 0 , 1]

m1 = random.randint(0, 1)
m2 = random.randint(0, 1)
print("Encrypting bits: ", m1, m2)

time.sleep(5)

e1 = AGCDEncrypt(m1, C)
e2 = AGCDEncrypt(m2, C)

print("Encrypted: ", e1, e2)

time.sleep(5)

dif = e1 - e2
for i in range(len(C)):
    for j in range(i+1, len(C)):
        sys.stdout.write("\rSearching: " + str(i) + " , " + str(j) +  " ")
        sys.stdout.flush()
        if dif == C[i] + C[j] or dif == C[i] - C[j] or dif == C[j] - C[i] or dif == -C[i] - C[j]:
            print("\nSuccess! Leaked randomness:")
            print(i, j)
            print("Encrypted bits are the same")
            exit(0)
        if dif == C[i] + C[j] + 1 or dif == C[i] - C[j] + 1 or dif == C[j] - C[i] + 1 or dif == -C[i] - C[j] + 1:
            print("\nSuccess! Leaked randomness:")
            print(i, j)
            print("Encrypted bits are different")
            exit(0)
        if dif == C[i] + C[j] - 1 or dif == C[i] - C[j] - 1 or dif == C[j] - C[i] - 1 or dif == -C[i] - C[j] - 1:
            print("\nSuccess! Leaked randomness:")
            print(i, j)
            print("Encrypted bits are different")
            exit(0)
