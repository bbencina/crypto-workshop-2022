import time

def RSAEncypt(m, n, e):
    return m ** e % n

def RSADecrypt(c, n, d):
    return c ** d % n

def EulerPhi(p, q):
    return (p - 1)*(q - 1)

def GCD(a,b):
    while b:
        a, b = b, a%b
    return a

def Inverse(x, n):
  b0 = n
  x0, x1 = 0, 1
  if n == 1: return 1

  while x > 1:
    try:
      q = x // n
      x, n = n, x%n
      x0, x1 = x1 - q * x0, x0
    except:
      print("Bad N values (check no common factors in N vals)")
      return 0
  if x1 < 0: x1 += b0
  return x1

p = int(input("Input p: "))
q = int(input("Input q: "))

n = p * q
print("Computed n =", p, "*", q, "=", n)

phi = EulerPhi(p, q)
print("Computed phi(n) =", phi)

e = int(input("Input e: "))

if GCD(e, phi) != 1:
    print("e has to be coprime to phi(n)!")
    exit(1)

print("e is indeed coprime to phi(n), computing inverse d...")

d = Inverse(e, phi)
print("Computed d =", d)

if e * d % phi != 1:
    print("The equation e*d % phi = 1 is not satisfied!")
    exit(1)

print("The equation e * d % phi =1 is indeed satisfied, proceeding with encryption...")

m = int(input("Input secret message: "))

if m >= n:
    print("m should be < n")
    exit(1)

c = RSAEncypt(m, n, e)

print("ENCRYPT: Computed ciphertext: ", c)

print("proceeding with decryption...")

mm = RSADecrypt(c, n, d)

print("DECRYPT: Computed plaintext: ", mm)

if m != mm:
    print("error: plaintexts are not equal")
    exit(1)

print("It works!")
