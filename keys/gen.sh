#!/bin/bash

# generate private key RSA2048
openssl genrsa -out private.pem 2048

# export public key
openssl rsa -in private.pem -outform PEM -pubout -out public.pem

# OpenSSH can also do this!
ssh-keygen -t rsa -b 4096 -C "demo key" -f demo_key

# GnuPG can ALSO do this!
mkdir -p tmp
GNUPGHOME=${PWD}/tmp gpg --full-generate-key
